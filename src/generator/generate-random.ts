import { Connection } from 'typeorm';
import {
  name,
  address,
  phone,
  random,
  datatype,
  date,
  lorem,
} from 'faker/locale/id_ID';
import {
  Buku,
  CabangPerpustakaan,
  Member,
  Pegawai,
  Peminjaman,
  Penerbit,
  Pengarang,
  Rak,
} from '../entity/perpustakaan';

export async function generateRandomData(
  db: Connection,
  scale: number = 1
): Promise<void> {
  const penerbit: Penerbit[] = Array.from(Array(scale * 10), () => {
    return {
      nama: `${name.firstName()} ${name.lastName()}`,
      alamat: `${address.streetAddress()} ${address.city()}`,
      telepon: `${phone.phoneNumber()}`,
    };
  });
  console.info('Saving Penerbit...');
  await db.getRepository(Penerbit).save(penerbit);

  const pengarang: Pengarang[] = Array.from(Array(scale * 10), () => {
    return {
      nama: `${name.firstName()} ${name.lastName()}`,
      jenisKelamin: datatype.boolean() ? 'L' : 'P',
      telepon: `${phone.phoneNumber()}`,
    };
  });
  console.info('Saving Pengarang...');
  await db.getRepository(Pengarang).save(pengarang);

  const rak: Rak[] = Array.from(Array(scale * 10), () => {
    return {
      kode: datatype.uuid(),
    };
  });
  console.info('Saving Rak...');
  await db.getRepository(Rak).save(rak);

  const cabang: CabangPerpustakaan[] = Array.from(Array(scale * 10), () => {
    return {
      nama: `${address.streetName()}`,
      alamat: `${address.streetAddress()} ${address.city()}`,
    };
  });
  console.info('Saving Cabang Perpustakaan...');
  await db.getRepository(CabangPerpustakaan).save(cabang);

  const member: Member[] = Array.from(Array(scale * 10), () => {
    return {
      nama: `${name.firstName()} ${name.lastName()}`,
      jenisKelamin: datatype.boolean() ? 'L' : 'P',
      alamat: `${address.streetAddress()} ${address.city()}`,
      telepon: `${phone.phoneNumber()}`,
    };
  });
  console.info('Saving Member...');
  await db.getRepository(Member).save(member);

  const pegawai: Pegawai[] = Array.from(Array(scale * 2), () => {
    return {
      nama: `${name.firstName()} ${name.lastName()}`,
      jenisKelamin: datatype.boolean() ? 'L' : 'P',
      telepon: `${phone.phoneNumber()}`,
      alamat: `${address.streetAddress()} ${address.city()}`,
    };
  });
  console.info('Saving Pegawai...');
  await db.getRepository(Pegawai).save(pegawai);

  const buku: Buku[] = Array.from(Array(scale * 100), () => {
    return {
      nama: lorem.words(5),
      sinopsis: lorem.sentences(3),
      idPengarang: random.arrayElement(pengarang).id,
      idRak: random.arrayElement(rak).id,
      kodeBarcode: random.alphaNumeric(16),
      idPenerbit: random.arrayElement(penerbit).id,
    };
  });
  console.info('Saving Buku...');
  await db.getRepository(Buku).save(buku);

  const _MS_PER_DAY = 1000 * 60 * 60 * 24;
  const dateDiffInDays = (a: Date, b: Date): number => {
    const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
    const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());
    return Math.floor(Math.abs(utc2 - utc1) / _MS_PER_DAY);
  };

  const peminjaman: Peminjaman[] = Array.from(Array(scale * 100), () => {
    const tanggalPinjam = date.past();
    const tanggalHarusKembali = new Date(
      new Date(tanggalPinjam).setMonth(tanggalPinjam.getMonth() + 1)
    );
    const afterHarusKembali = new Date(
      new Date(tanggalHarusKembali).setMonth(tanggalHarusKembali.getMonth() + 1)
    );
    const tanggalKembali = datatype.boolean()
      ? date.between(tanggalPinjam, afterHarusKembali)
      : undefined;

    return {
      tanggalPinjam,
      tanggalHarusKembali,
      tanggalKembali,
      denda:
        tanggalKembali && tanggalKembali > tanggalHarusKembali
          ? dateDiffInDays(tanggalKembali, tanggalHarusKembali) * 1000
          : 0,
      status: tanggalKembali ? 'SUDAH_KEMBALI' : 'BELUM_KEMBALI',
      idCabang: random.arrayElement(cabang).id,
      idMember: random.arrayElement(member).id,
      idPegawai: random.arrayElement(pegawai).id,
      buku: random.arrayElements(buku),
    };
  });
  console.info('Saving Peminjaman...');
  await db.getRepository(Peminjaman).save(peminjaman);
}
