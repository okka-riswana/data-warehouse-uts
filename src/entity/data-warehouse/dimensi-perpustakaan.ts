import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('dim_perpustakaan')
export class DimensiPerpustakaan {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column({ name: 'nama_perpustakaan' })
  nama: string;

  @Column({ name: 'alamat' })
  alamat: string;

  @CreateDateColumn({ name: 'dibuat_pada' })
  dibuatPada: Date;
}
