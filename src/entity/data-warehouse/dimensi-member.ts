import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('dim_member')
export class DimensiMember {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column({ name: 'nama_member' })
  nama: string;

  @Column({ name: 'alamat' })
  alamat: string;

  @Column({ name: 'jenis_kelamin' })
  jenisKelamin: string;

  @CreateDateColumn({ name: 'dibuat_pada' })
  dibuatPada: Date;
}
