import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm';
import { DimensiBuku } from './dimensi-buku';
import { DimensiMember } from './dimensi-member';
import { DimensiPerpustakaan } from './dimensi-perpustakaan';
import { FaktaPeminjamanBulan } from './fakta-peminjaman-bulan';

@Entity('fakta_peminjaman_tahun')
export class FaktaPeminjamanTahun {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column({ name: 'tanggal' })
  tanggal: Date;

  @Column({ name: 'jumlah' })
  jumlah: Number;

  @CreateDateColumn({ name: 'dibuat_pada' })
  dibuatPada: Date;

  @ManyToOne((_) => DimensiBuku)
  @JoinColumn({ name: 'id_dim_buku' })
  dimBuku?: DimensiBuku;

  @Column({ name: 'id_dim_buku' })
  @RelationId((fpb: FaktaPeminjamanBulan) => fpb.dimBuku)
  idDimBuku: string;

  @ManyToOne((_) => DimensiMember)
  @JoinColumn({ name: 'id_dim_member' })
  dimMember?: DimensiMember;

  @Column({ name: 'id_dim_member' })
  @RelationId((fpb: FaktaPeminjamanBulan) => fpb.dimMember)
  idDimMember: string;

  @ManyToOne((_) => DimensiPerpustakaan)
  @JoinColumn({ name: 'id_dim_perpustakaan' })
  dimPerpustakaan?: DimensiPerpustakaan;

  @Column({ name: 'id_dim_perpustakaan' })
  @RelationId((fpb: FaktaPeminjamanBulan) => fpb.dimPerpustakaan)
  idDimPerpustakaan: string;
}
