import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('dim_buku')
export class DimensiBuku {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column({ name: 'nama_buku' })
  buku: string;

  @Column({ name: 'nama_pengarang' })
  pengarang: string;

  @Column({ name: 'nama_penerbit' })
  penerbit: string;

  @CreateDateColumn({ name: 'dibuat_pada' })
  dibuatPada: Date;
}
