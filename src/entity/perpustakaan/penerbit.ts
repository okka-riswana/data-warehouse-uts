import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('penerbit')
export class Penerbit {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column({ name: 'nama_penerbit' })
  nama: string;

  @Column({ name: 'alamat' })
  alamat: string;

  @Column({ name: 'telepon' })
  telepon: string;
}
