import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  JoinColumn,
  RelationId,
  OneToOne,
  ManyToMany,
} from 'typeorm';
import { Rak } from '.';
import { Peminjaman } from './peminjaman';
import { Penerbit } from './penerbit';
import { Pengarang } from './pengarang';

@Entity('buku')
export class Buku {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column({ name: 'nama_buku' })
  nama: string;

  @Column({ name: 'sinopis' })
  sinopsis: string;

  @Column({ name: 'kode_barcode' })
  kodeBarcode: string;

  @ManyToOne((_) => Pengarang)
  @JoinColumn({ name: 'id_pengarang' })
  pengarang?: Pengarang;

  @Column({ name: 'id_pengarang' })
  @RelationId((b: Buku) => b.pengarang)
  idPengarang?: number;

  @ManyToOne((_) => Penerbit)
  @JoinColumn({ name: 'id_penerbit' })
  penerbit?: Penerbit;

  @Column({ name: 'id_penerbit' })
  @RelationId((b: Buku) => b.penerbit)
  idPenerbit?: number;

  @ManyToOne((_) => Rak)
  @JoinColumn({ name: 'id_rak' })
  rak?: Rak;

  @Column({ name: 'id_rak' })
  @RelationId((b: Buku) => b.rak)
  idRak?: number;

  @ManyToMany((_) => Peminjaman, (p) => p.buku, { onDelete: 'SET NULL' })
  peminjaman?: Peminjaman[];
}
