import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('pengarang')
export class Pengarang {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column({ name: 'nama_pengarang' })
  nama: string;

  @Column({ name: 'jenis_kelamin' })
  jenisKelamin: string;

  @Column({ name: 'telepon' })
  telepon: string;
}
