import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('rak')
export class Rak {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column({ name: 'kode_rak' })
  kode: string;
}
