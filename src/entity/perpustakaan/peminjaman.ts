import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
  JoinTable,
  JoinColumn,
  ManyToOne,
  RelationId,
} from 'typeorm';
import { Buku } from './buku';
import { CabangPerpustakaan } from './cabang-perpustakaan';
import { Member } from './member';
import { Pegawai } from './pegawai';

@Entity('peminjaman')
export class Peminjaman {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column({ name: 'tanggal_pinjam' })
  tanggalPinjam: Date;

  @Column({ name: 'tanggal_harus_kembali' })
  tanggalHarusKembali: Date;

  @Column({ name: 'tanggal_kembali', nullable: true })
  tanggalKembali?: Date;

  @Column({ name: 'denda', nullable: true })
  denda?: number;

  @Column({ name: 'status' })
  status: string;

  @ManyToOne((_) => Pegawai)
  @JoinColumn({ name: 'id_pegawai' })
  pegawai?: Pegawai;

  @Column({ name: 'id_pegawai' })
  @RelationId((p: Peminjaman) => p.pegawai)
  idPegawai?: number;

  @ManyToOne((_) => Member)
  @JoinColumn({ name: 'id_member' })
  member?: Member;

  @Column({ name: 'id_member' })
  @RelationId((p: Peminjaman) => p.member)
  idMember?: number;

  @ManyToOne((_) => CabangPerpustakaan)
  @JoinColumn({ name: 'id_cabang_perpustakaan' })
  cabang?: CabangPerpustakaan;

  @Column({ name: 'id_cabang_perpustakaan' })
  @RelationId((p: Peminjaman) => p.cabang)
  idCabang?: number;

  @ManyToMany((_) => Buku, (b) => b.peminjaman, {
    nullable: true,
    onDelete: 'SET NULL',
  })
  @JoinTable({ name: 'detail_peminjaman' })
  buku?: Buku[];
}
