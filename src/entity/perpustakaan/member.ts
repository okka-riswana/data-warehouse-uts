import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('member')
export class Member {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column({ name: 'nama_member' })
  nama: string;

  @Column({ name: 'jenis_kelamin' })
  jenisKelamin: string;

  @Column({ name: 'alamat' })
  alamat: string;

  @Column({ name: 'telepon' })
  telepon: string;
}
