import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('cabang_perpustakaan')
export class CabangPerpustakaan {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column({ name: 'nama_perpustakaan' })
  nama: string;

  @Column({ name: 'alamat' })
  alamat: string;
}
