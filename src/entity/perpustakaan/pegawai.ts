import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('pegawai')
export class Pegawai {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column({ name: 'nama_pegawai' })
  nama: string;

  @Column({ name: 'jenis_kelamin' })
  jenisKelamin: string;

  @Column({ name: 'alamat' })
  alamat: string;

  @Column({ name: 'telepon' })
  telepon: string;
}
