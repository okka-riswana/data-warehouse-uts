import 'reflect-metadata';
import { Connection, createConnection } from 'typeorm';
import { generateRandomData } from './generator';

(async () => {
  const oltpConn = await createConnection({
    name: 'oltp',
    type: 'postgres',
    host: 'localhost',
    port: 5432,
    username: 'postgres',
    password: 'postgres',
    database: 'oltp',
    entities: [__dirname + '/entity/perpustakaan/**/*.ts'],
    synchronize: true,
  });

  // generateRandomData(oltpConn);

  const olapConn = await createConnection({
    name: 'olap',
    type: 'postgres',
    host: 'localhost',
    port: 5433,
    username: 'postgres',
    password: 'postgres',
    database: 'olap',
    entities: [__dirname + '/entity/data-warehouse/**/*.ts'],
    synchronize: true,
  });
})();
